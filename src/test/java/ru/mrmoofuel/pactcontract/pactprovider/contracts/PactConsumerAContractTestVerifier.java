package ru.mrmoofuel.pactcontract.pactprovider.contracts;

import au.com.dius.pact.provider.junit.Consumer;
import au.com.dius.pact.provider.junit.IgnoreNoPactsToVerify;
import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.loader.PactBroker;
import au.com.dius.pact.provider.junit.target.Target;
import au.com.dius.pact.provider.junit.target.TestTarget;
import au.com.dius.pact.provider.spring.SpringRestPactRunner;
import au.com.dius.pact.provider.spring.target.SpringBootHttpTarget;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author dmitriy
 * @since 30.05.19
 */
@RunWith(SpringRestPactRunner.class)
@PactBroker
@Provider("pact-provider")
@Consumer("pact-consumer-a")
@IgnoreNoPactsToVerify
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PactConsumerAContractTestVerifier {

    @TestTarget
    public final Target target = new SpringBootHttpTarget();
}
