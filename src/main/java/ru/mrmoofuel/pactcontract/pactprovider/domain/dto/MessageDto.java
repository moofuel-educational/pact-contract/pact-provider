package ru.mrmoofuel.pactcontract.pactprovider.domain.dto;

import lombok.Data;

/**
 * @author dmitriy
 * @since 31.05.19
 */
@Data
public class MessageDto {
    private String message;
}
