package ru.mrmoofuel.pactcontract.pactprovider.domain.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * @author dmitriy
 * @since 29.05.19
 */
@Data
@Setter(AccessLevel.NONE)
@RequiredArgsConstructor
public class HelloDto {

    private final String hello;

}
