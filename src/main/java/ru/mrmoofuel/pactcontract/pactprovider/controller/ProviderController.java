package ru.mrmoofuel.pactcontract.pactprovider.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.mrmoofuel.pactcontract.pactprovider.domain.dto.HelloDto;
import ru.mrmoofuel.pactcontract.pactprovider.domain.dto.MessageDto;

/**
 * @author dmitriy
 * @since 29.05.19
 */
@RestController
@RequestMapping("/provider")
@RequiredArgsConstructor
public class ProviderController {


    @GetMapping("/hello")
    public HelloDto sayHello() {
        return new HelloDto("Hello");
    }

    @PostMapping("/chat")
    public MessageDto answerThis(@RequestBody MessageDto messageDto) {
        if ("lol".equalsIgnoreCase(messageDto.getMessage())) {
            final MessageDto kek = new MessageDto();
            kek.setMessage("Hello");
            return kek;
        }
        final MessageDto uncertain = new MessageDto();
        uncertain.setMessage("wat");
        return uncertain;
    }
}
